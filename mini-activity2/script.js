let items = document.getElementsByClassName("colorMe")
// Same as querySelectorAll('.sample')

console.log(items)


for(const item of items) { 		// We used the for of method
	// console.log(item)			// For each item of items, gagawin ito ni for of
		// Output is it will show 6 items
	const changeColor = () =>{
		let enterColor = prompt("Enter Color")
		item.style.background = enterColor
		//item.innerHTML = enterColor 
			// this will add a text in a selected box
	}
	item.addEventListener("click", changeColor)
}
// This will add color sa ibat-ibang boxes when clicking a specific box